<?php include 'head.php'; ?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
  <meta charset="utf-8">
  <title>Simple Polylines</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/modules/variable-pie.js"></script>
  <style>
    /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
    #map {
      height: 100%;
    }

    /* Optional: Makes the sample page fill the window. */
    html,
    body {
      height: 100%;
      margin: 0;
      padding: 0;
    }

    #floating-panel {
      position: absolute;
      top: 150px;
      left: 2%;
      z-index: 5;
      text-align: center;
    }
  </style>
</head>

<body>
<nav class="navbar navbar-expand-lg navbar-light bg-primary">
  <a class="navbar-brand text-light" href="#">CMU transit</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link text-light" href="main.php">Overview</a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-light" href="map.php">Heatmap</a>
    </ul>
    <form action="" method="POST" class="form-inline my-2">
            <input class="form-control mr-sm-2" type="datetime-local" value="2019-11-21T16:00" name="datetime-start">
            <input class="form-control mar-sm-2" type="datetime-local" value="2019-11-21T17:00" name="datetime-end">
            <button class="btn btn-success" type="submit">Submit</button>

        </form>
  </div>
</nav>
  <?php
  $start = (int) strtotime($_POST['datetime-start']);
  $end = (int) strtotime($_POST['datetime-end']);
  $string_query = "";
  //echo $route. "\n";

  $datetime_start = date('Y-m-d H:i:s', $start);
  $datetime_end = date('Y-m-d H:i:s', $end);
  $date = date('Y-m-d', $start);
  //echo $date_start . "\n" . $date_end;
  $db2 = new PDO('pgsql:host=localhost;port=5432;dbname=log;user=postgres;password=123456');
  $sql1 = "SELECT * FROM stat_in_hour WHERE datetime BETWEEN '$datetime_start' AND '$datetime_end' ";

  $geton =array(0,0,0,0,0);
  $distance =array(0,0,0,0,0);

  foreach ($db2->query($sql1) as $row) {
    if($row['route'] == 1) {$geton[0] += $row['geton'];$distance[0] += $row['distance'];}
    if($row['route'] == 2) {$geton[1] += $row['geton'];$distance[1] += $row['distance'];}
    if($row['route'] == 3) {$geton[2] += $row['geton'];$distance[2] += $row['distance'];}
    if($row['route'] == 4) {$geton[3] += $row['geton'];$distance[3] += $row['distance'];}
    if($row['route'] == 5) {$geton[4] += $row['geton'];$distance[4] += $row['distance'];}
  }

  
  /// ได้ DATA มาแล้ว เหลือเอาลง chart
  ?>
  <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/highcharts-more.js"></script>
  <script src="https://code.highcharts.com/modules/exporting.js"></script>

  <div class="container">
    <div class="row">
      <div class="col-sm p-5 bg-primary">
       <h4 class="my-0 font-weight-normal text-light">สายสีเขียว</h4> 
       <h4 class="my-0 font-weight-normal text-light"><?php echo $geton[0]; ?></h4>
       <h4 class="my-0 font-weight-normal text-light"><?php echo round($distance[0],2); ?></h4>
      </div>
      <div class="col-sm p-5  bg-light">
      <h4 class="my-0 font-weight-normal">สายสีส้ม</h4> 
      <h4 class="my-0 font-weight-normal"><?php echo $geton[1]; ?></h4> 
      <h4 class="my-0 font-weight-normal"><?php echo round($distance[1],2); ?></h4>
      </div>
      <div class="col-sm p-5 bg-primary">
      <h4 class="my-0 font-weight-normal text-light">สายสีแดง</h4> 
      <h4 class="my-0 font-weight-normal text-light"><?php echo $geton[2]; ?></h4>
      <h4 class="my-0 font-weight-normal text-light"><?php echo round($distance[2],2); ?></h4>
      </div>
      <div class="col-sm p-5 bg-light">
      <h4 class="my-0 font-weight-normal">สายสีฟ้า</h4> 
      <h4 class="my-0 font-weight-normal"><?php echo $geton[3]; ?></h4>
      <h4 class="my-0 font-weight-normal"><?php echo round($distance[3],2); ?></h4>
      </div>
      <div class="col-sm p-5  bg-primary">
      <h4 class="my-0 font-weight-normal text-light">สายสีม่วง</h4> 
      <h4 class="my-0 font-weight-normal text-light"><?php echo $geton[4]; ?></h4>
      <h4 class="my-0 font-weight-normal text-light"><?php echo round($distance[4],2); ?></h4>
      </div>
    </div>
    <div class="row">
      <div class="col-sm p-2 mb-4 bg-light" id="bar">
      </div>
      <div class="col-sm p-2 mb-4 bg-light "id="circle">
      </div>
    </div>
  </div>

  <script>
    var chart = Highcharts.chart('bar', {

      title: {
        text: 'จำนวนคนขึ้นรถ'
      },

      subtitle: {
        text: 'สายการเดินรถ'
      },
      colors: [
        'green',
        'orange',
        'red',
        'blue',
        'purple'
      ],
      xAxis: {
        categories: ['สาย 1 สีเขียว', 'สาย 1 สีส้ม', 'สาย 3 สีแดง', 'สาย 4 สีฟ้า', 'สาย 5 สีม่วง']
      },

      series: [{
        type: 'column',
        colorByPoint: true,
        data: [<?php echo $geton[0]; ?>, <?php echo $geton[1]; ?>, <?php echo $geton[2]; ?>, <?php echo $geton[3]; ?>, <?php echo $geton[4]; ?>],
        showInLegend: false
      }]

    });

    Highcharts.chart('circle', {
      chart: {
        type: 'variablepie'
      },
      title: {
        text: 'สัดส่วนจำนวนผู้โดยสารและระยะทาง'
      },
      tooltip: {
        headerFormat: '',
        pointFormat: '<span style="color:{point.color}">\u25CF</span> <b> {point.name}</b><br/>' +
          'จำนวนผู้โดยสาร: <b>{point.y}</b><br/>' +
          'ระยะทาง(เมตร): <b>{point.z}</b><br/>'
      },
      series: [{
        minPointSize: 10,
        innerSize: '60%',
        zMin: 0,
        name: 'countries',
        data: [{
          name: 'สาย 1 สีเขียว',
          color: 'green',
          y: <?php echo $geton[0]; ?>,
          z: <?php echo round($distance[0],2); ?>
        }, {
          name: 'สาย 2 สีส้ม',
          color: 'orange',
          y: <?php echo $geton[1]; ?>,
          z: <?php echo round($distance[1],2); ?>
        }, {
          name: 'สาย 3 สีแดง',
          color: 'red',
          y: <?php echo $geton[2]; ?>,
          z: <?php echo round($distance[2],2); ?>
        }, {
          name: 'สาย 4 สีฟ้า',
          color: 'blue',
          y: <?php echo $geton[3]; ?>,
          z: <?php echo round($distance[3],2); ?>
        }, {
          name: 'สาย 5 สีม่วง',
          color: 'purple',
          y: <?php echo $geton[4]; ?>,
          z: <?php echo round($distance[4],2); ?>
        }]
      }]
    });
  </script>




</body>

</html>