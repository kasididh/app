<nav class="navbar navbar-expand-lg bg-primary">
  <a class="navbar-brand text-light" href="main.php">CMU transit</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link text-light" href="main.php">Overview</a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-light" href="map.php">Heatmap</a>
    </ul>
    <form action="" method="POST" class="form-inline my-2">
            <div class="input-group">
                <div class="input-group-prepend">
                    <label class="input-group-text" for="inputGroupSelect01">สายรถ</label>
                </div>
                <select class="custom-select mr-sm-2" name="route">
                    <option selected value="0">ทั้งหมด</option>
                    <option value="1">สายสีเขียว</option>
                    <option value="2">สายสีส้ม</option>
                    <option value="3">สายสีแดง</option>
                    <option value="4">สายสีฟ้า</option>
                    <option value="5">สายสีม่วง</option>
                </select>
            </div>
            <input class="form-control mr-sm-2" type="datetime-local" value="2019-11-21T16:00" name="datetime-start">
            <input class="form-control mar-sm-2" type="datetime-local" value="2019-11-21T17:00" name="datetime-end">
            <button class="btn btn-success" type="submit">Submit</button>

        </form>
  </div>
</nav>