<?php
require 'vendor/autoload.php';
$client1 = new MongoDB\Client;
$db1 = $client1->CMU;
$collection1 = $db1->log;
date_default_timezone_set("Asia/Bangkok");
$hour = date('H')-1;
$hour2 = date('H');
$date = date('Y-m-d');
$timeString1 = '2019-11-21 17:00:00';
$timeString2 = '2019-11-21 18:00:00';

$start = (int)strtotime($timeString1)*1000;
$end = (int)strtotime($timeString2)*1000;
$filter = ['route' =>  ['$lt' => 6]];
$data = $collection1->find($filter);
// $myJSON = json_encode($data1);
// ไม่ได้แยก bus

foreach ($data as $document) {
    $source = (int)(string)$document['source_time'];
    if($source >= $start && $source <= $end){
        $array[] = array(
            'lat' => $document['lat'],
            'lng' => $document['lng'],
            'route' => $document['route']
        );
    }
}

echo json_encode($array);

?>